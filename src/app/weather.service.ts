import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Weather } from './interfaces/weather';
import { WeatherRaw } from './interfaces/weather-raw';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL ="https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY = "8ab2692086ca18c8b0bd09f77f4ab785";
  private IMP ="units=metric";

  constructor(private http:HttpClient) { }

  searchWeatherData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transFormWeatherData(data)),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server Error');
  }

  private transFormWeatherData(data:WeatherRaw):Weather{
    return {
      name:data.name,
      country:data.sys.country,
      image:`http://api.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon

    }
  }

}
