import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Posts } from '../interfaces/posts';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  userId:number;
  id:number;
  title:string;
  body:string;
  PostsData$:Observable<Posts>
  hasError:boolean = false;

  
  constructor(private route: ActivatedRoute, private postsService:PostsService) { }

  
  ngOnInit(): void {
    this.PostsData$ = this.postsService.searchPostsData();
    this.PostsData$.subscribe(
      data => {
                this.userId=data.userId;
                this.id= data.id;
                this.title=data.title;
                this.body= data.body;
              },
      error => {
        console.log(error.message);
        this.hasError=true;
      })
  }

}
