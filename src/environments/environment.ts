// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBKIaqkqkQjlXv5rGDYKaxmNFQUNGR_ZZU",
    authDomain: "hello-jce-maor.firebaseapp.com",
    databaseURL: "https://hello-jce-maor.firebaseio.com",
    projectId: "hello-jce-maor",
    storageBucket: "hello-jce-maor.appspot.com",
    messagingSenderId: "831767741620",
    appId: "1:831767741620:web:51e904ab5560b3efdac3e1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
